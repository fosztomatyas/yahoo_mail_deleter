import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

import pages.*;

public class MyStoreTest {
	
	static WebDriver driver;
    static  YahooMailPage yahooMailPage;

	@BeforeAll
	static void setupTest() {
		driver = new ChromeDriver();
	}

	@BeforeEach
    void setUp() {
        yahooMailPage = new YahooMailPage((driver));
        driver.navigate().to("http://mail.yahoo.com");
        driver.manage().window().maximize();
    }

    @AfterAll
    public static void quitDriver() {
        driver.quit();
    }

    @Test
    public void DeleteAllMail() {
        yahooMailPage.navigateTo();
        // For me, Yahoo bugged after my e-mails got under 1000, and the script would not work anymore.
	    while (yahooMailPage.getMailCount() > 100) {
	        try {
                yahooMailPage.selectAllMail();
                yahooMailPage.clickDeleteButton();
                yahooMailPage.clickWarningOk();
            } catch (Exception ignored) {}
        }
    }


}