package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YahooMailPage {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//span[@class='D_F']//button[@class='c27KHO0_n b_0 M_0 i_0 I_T y_Z2uhb3X A_6EqO r_P C_q cvhIH6_T ir3_1JO2M7 P_0']")
    private WebElement selectAllCheckbox;

    @FindBy(id = "login-username")
    private WebElement loginField;

    @FindBy(id = "login-signin")
    private WebElement loginButton;

    @FindBy(xpath = "//input[@id='login-passwd']")
    private WebElement passInput;

    @FindBy(xpath = "//li[@name='delete']//button[@class='c27KHO0_n b_0 M_0 i_0 I_T y_Z2uhb3X A_6EqO r_P C_q cvhIH6_T ir3_1JO2M7 P_eo6']")
    private WebElement deleteButton;

    @FindBy(xpath = "//button[@class='P_1EudUu C_52qC r_P y_Z2uhb3X A_6EqO cvhIH6_T ir3_1JO2M7 cZ11XJIl_0 k_w e_dRA D_X M_6LEV o_v p_R V_M t_C cZ1RN91d_n u_e69 i_3qGKe H_A cn_dBP cg_FJ l_Z2aVTcY j_n S_n S4_n I_Z2aVTcY I3_Z2bdAhD l3_Z2bdIi1 I0_Z2bdAhD l0_Z2bdIi1 I4_Z2bdAhD l4_Z2bdIi1']")
    private WebElement warningOkButton;

    @FindBy(xpath = "//span[@class='A_6DUj P_Z2rzCvT H_6FIA i_6FIA t_C e_3mS2U C_Z281SGl I_T u_b']")
    private WebElement inboxMailCount;


    public YahooMailPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 1), this);
        wait = new WebDriverWait(driver, 5);
    }

    public void navigateTo() {
        loginField.sendKeys("email@yahoo.com");
        loginButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(passInput));
        passInput.sendKeys("your password here");
        loginButton.click();
    }

    /*
    Elements sometimes intercept clicks. I was lazy/unable to solve this without errors.
    * */
    public void selectAllMail() {
//        wait.until(ExpectedConditions.elementToBeClickable(selectAllCheckbox));
        selectAllCheckbox.click();
    }

    public void clickDeleteButton() {
//        wait.until(ExpectedConditions.elementToBeClickable(deleteButton));
        deleteButton.click();
    }

    public void clickWarningOk() {
//        WebDriverWait waitt = new WebDriverWait(driver, 5);
        try {
//            waitt.until(ExpectedConditions.elementToBeClickable(warningOkButton));
            if(warningOkButton.isDisplayed()) {
                warningOkButton.click();
            }
        }
        catch (Exception ignored) {} // it's a mess, so I mopped it under the carpet

    }

    public int getMailCount() {
        String mailString = inboxMailCount.getText();
        if (mailString.contains("+")) {
            mailString = mailString.substring(0, mailString.length() - 1);
        }
        return Integer.parseInt(mailString);
    }
}
